# Cookiecutter template for a Guerrilla Analytics project

This `cookiecutter` template provides the foundational structure for a data analytics project
as proposed by Enda Ridge in his book "Guerrilla Analytics".

This is after asking for some parameters such as project and user names it generates a structure of the following basic project structure:
```
/project_slug
├─ README.md (basic project information)
├─ builds
    ├─ ...
├─ data
    ├─ datalog.csv
    ├─ [D001]
    ├─ ...
├─ workspaces
    ├─ username
    ├─ ...
├─ wp
    ├─ work-products-log.csv
    ├─ [0010]
    ├─ ...
```

Optionally, the resulting project is initialized as a Git project.

There are more `cookiecutter` templates available for various purposes within such a project.
The following are available yet:

* [Add a new data source](https://git.rwth-aachen.de/guerrilla-analytics/data)
* [Add a new work product](https://git.rwth-aachen.de/guerrilla-analytics/work-product)

Certainly, you are free and even encouraged to tailor the provided templates as well as this one here to your own needs by deriving from this GitLab project and incorporating much more specific details and handlings.
*Just think about sharing your templates again, it might be worthwhile for others!*

