import subprocess
import os

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)

if __name__ == '__main__':
    # Remove unnecessary files (only needed to allow for empty directories at Git origin)
    os.remove(os.path.join(PROJECT_DIRECTORY, "builds/.gitkeep"))
    os.remove(os.path.join(PROJECT_DIRECTORY, "workspaces/" + '{{ cookiecutter.username }}' + "/.gitkeep"))
    # Initialize git if required
    if '{{ cookiecutter.use_git }}'.lower() in ('y', 'yes'):
        subprocess.call(['git', 'init'])
        subprocess.call(['git', 'add', '*'])
        subprocess.call(['git', 'commit', '-m', 'Initial commit'])