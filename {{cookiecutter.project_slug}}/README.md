# {{cookiecutter.project_name}}

This project "{{cookiecutter.project_name}}" has been established from the [Guerrilla Analytics "project" cookiecutter template](https://git.rwth-aachen.de/guerrilla-analytics/project) by {{cookiecutter.username}}.

## Short Description
{{cookiecutter.project_desc}}

## Basic Guerrilla Analytics Structure

### Data `/data`
* All source **data** will be collected within `/data/DXXX`, one data source/set per directory.
* Additional information are added there within the subdirectory `supporting`.
* All data sources/sets need to be added to the list at `/data/datalog.csv`.
* You can use the [Guerrilla Analytics "data" cookiecutter template](https://git.rwth-aachen.de/guerrilla-analytics/data) to establish a new data source simply via the shortcut 
    `adddata.sh`

### Results aka "work products" `/wp`
* Results are produced via "work products" within the directory again uniquely identified: `/wp/YYYY`
* Again a recording of available work products is kept at `/wp/work-products-log.csv`
* You can use the [Guerrilla Analytics "work-product" cookiecutter template](https://git.rwth-aachen.de/guerrilla-analytics/work-product) to establish a new work product via the shortcut
    `addwp.sh`

### Builds `/builds`
* Consolidated knowledge ("data" or "service builds") are stored at `/builds/`
* This consolidated knowledge can safely be referred from workspaces and work products.

### Workspaces `/workspaces`
* During trials and development users can user their individual workspaces at `/workspaces/USERNAME`
* Only finalized code should be moved to the work products area `/wp/...`
